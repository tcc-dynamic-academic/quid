package br.ueg.unucet.quidgerenciadorservico;

import org.springframework.aop.framework.Advised;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import br.ueg.unucet.quid.controladores.ServicoControle;
import br.ueg.unucet.quid.controladores.TipoMembroControle;
import br.ueg.unucet.quid.interfaces.IArtefatoPreenchidoControle;
import br.ueg.unucet.quid.interfaces.IArtefatoServicoControle;
import br.ueg.unucet.quid.servicos.QuidService;


@Service("quidGerenciadorServico")
public class QuidGerenciadorServico {

	private static QuidGerenciadorServico instancia = null;
	private static ApplicationContext appContext = QuidService.obterInstancia().getAppContext();
	
	private TipoMembroControle tipoMembroControle ;
	private ServicoControle servicoControle ;
	@Autowired
	private IArtefatoServicoControle artefatoServicoControle;
	@Autowired
	private IArtefatoPreenchidoControle artefatoPreenchidoControle;
	
	
	
	public TipoMembroControle getTipoMembroControle() {
		return tipoMembroControle;
	}

	public void setTipoMembroControle(TipoMembroControle tipoMembroControle) {
		this.tipoMembroControle = tipoMembroControle;
	}

	public ServicoControle getServicoControle() {
		return servicoControle;
	}

	public void setServicoControle(ServicoControle servicoControle) {
		this.servicoControle = servicoControle;
	}

	
	
	private QuidGerenciadorServico()
	{
	}
	
	public static QuidGerenciadorServico getInstancia(){
		if (instancia == null)
		{
			instancia = (QuidGerenciadorServico) appContext.getBean("quidGerenciadorServico");
			try {
				instancia.setTipoMembroControle( (TipoMembroControle) ((Advised)appContext.getBean("TipoMembroControle")).getTargetSource().getTarget());
				instancia.setServicoControle( (ServicoControle) ((Advised)appContext.getBean("ServicoControle")).getTargetSource().getTarget());
				//this.artefatoServicoControle = (ArtefatoServicoControle) ((Advised)appContext.getBean("ArtefatoServicoControle")).getTargetSource().getTarget();
			} catch (BeansException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return instancia;
		}
		else
		{
			return instancia;
		}
	}

	public IArtefatoServicoControle getArtefatoServicoControle() {
		return artefatoServicoControle;
	}

	public void setArtefatoServicoControle(
			IArtefatoServicoControle artefatoServicoControle) {
		this.artefatoServicoControle = artefatoServicoControle;
	}

	public IArtefatoPreenchidoControle getArtefatoPreenchidoControle() {
		return artefatoPreenchidoControle;
	}

	public void setArtefatoPreenchidoControle(
			IArtefatoPreenchidoControle artefatoPreenchidoControle) {
		this.artefatoPreenchidoControle = artefatoPreenchidoControle;
	}
	
}
