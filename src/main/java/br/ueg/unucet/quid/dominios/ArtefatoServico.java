package br.ueg.unucet.quid.dominios;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.ueg.unucet.quid.extensao.dominios.Identificavel;
import br.ueg.unucet.quid.extensao.enums.MomentosDisparoServicoEnum;

/**
 * Classe que representa o mapeamento ArtefatoServico dentro do framework.
 * @author QUID
 *
 */
@Entity
@Table(name="artefato_servico")
public class ArtefatoServico extends Identificavel{
	@ManyToOne
	private Artefato artefato;
	/**
	 * Servico do mapeamento.
	 */
	@ManyToOne
	private Servico servico;
	/**
	 * Servico anterior que sera executado antes do atributo servico.
	 */
	@ManyToOne
	private Servico servicoAnterior;
	/**
	 * Servico posterior que sera executa depois do atributo servico.
	 */
	@ManyToOne
	private Servico servicoProximo;
	/**
	 * Atributo que informa de o servico anterior e obrigatoria a execucao. Caso seje o atributo servico
	 * so sera executado caso o servico anterior retorne sucesso de execucao.
	 */
	private Boolean anteriorObrigatorio;
	/**
	 * Versao do serviço no Artefato
	 */
	private Integer versao;
	
	/**
	 * Vetor de bytes dos parametros do servico.
	 */
	private byte[] parametrosServico;
	
	@Enumerated(EnumType.STRING)
	private MomentosDisparoServicoEnum momentoDisparo;

	/**
	 * Membro framework do mapeamento.
	 */
	@ManyToOne
	private MembroFramework membroFramework;
	
	//GETTERS AND SETTERS
	public Artefato getArtefato() {
		return artefato;
	}
	public void setArtefato(Artefato artefato) {
		this.artefato = artefato;
	}
	public Servico getServico() {
		return servico;
	}
	public void setServico(Servico servico) {
		this.servico = servico;
	}
	public byte[] getParametrosServico() {
		return parametrosServico;
	}
	public void setParametrosServico(byte[] parametrosServico) {
		this.parametrosServico = parametrosServico;
	}
	public Servico getServicoAnterior() {
		return servicoAnterior;
	}
	public void setServicoAnterior(Servico servicoAnterior) {
		this.servicoAnterior = servicoAnterior;
	}
	public Servico getServicoProximo() {
		return servicoProximo;
	}
	public void setServicoProximo(Servico servicoProximo) {
		this.servicoProximo = servicoProximo;
	}
	/**
	 * @return the anteriorObrigatorio
	 */
	public Boolean getAnteriorObrigatorio() {
		return anteriorObrigatorio;
	}
	/**
	 * @param anteriorObrigatorio the anteriorObrigatorio to set
	 */
	public void setAnteriorObrigatorio(Boolean anteriorObrigatorio) {
		this.anteriorObrigatorio = anteriorObrigatorio;
	}
	public MomentosDisparoServicoEnum getMomentoDisparo() {
		return momentoDisparo;
	}
	public void setMomentoDisparo(MomentosDisparoServicoEnum momentoDisparo) {
		this.momentoDisparo = momentoDisparo;
	}
	public MembroFramework getMembroFramework() {
		return membroFramework;
	}
	public void setMembroFramework(MembroFramework membroFramework) {
		this.membroFramework = membroFramework;
	}
	public Integer getVersao() {
		return versao;
	}
	public void setVersao(Integer versao) {
		this.versao = versao;
	}
	
	
	
}
