package br.ueg.unucet.quid.dominios;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.ueg.unucet.quid.extensao.dominios.Identificavel;

/**
 * POJO que representa o ArtefatoModelo com seus valores a ser persistido como
 * ArtefatoPreenchido no Serviço de Persistência
 * 
 * @author Diego
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name="artefato_preenchido")
public class ArtefatoPreenchido extends Identificavel {
	
	/**
	 * ID do Artefato
	 */
	private Long artefato;
	/**
	 * ID do Modelo
	 */
	private Long modelo;
	/**
	 * ID do usuário que solicitou a ação
	 */
	private Long usuario;
	/**
	 * Versão do ArtefatoPreenchido
	 */
	private Integer versao;
	/**
	 * Revisão do ArtefatoPreenchido
	 */
	private Integer revisao;

	/**
	 * @return the artefato
	 */
	public Long getArtefato() {
		return artefato;
	}

	/**
	 * @param artefato
	 *            the artefato to set
	 */
	public void setArtefato(Long artefato) {
		this.artefato = artefato;
	}

	/**
	 * @return the modelo
	 */
	public Long getModelo() {
		return modelo;
	}

	/**
	 * @param modelo
	 *            the modelo to set
	 */
	public void setModelo(Long modelo) {
		this.modelo = modelo;
	}

	/**
	 * @return the usuario
	 */
	public Long getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	public void setUsuario(Long usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the versao
	 */
	public Integer getVersao() {
		return versao;
	}

	/**
	 * @param versao
	 *            the versao to set
	 */
	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	/**
	 * @return the revisao
	 */
	public Integer getRevisao() {
		return revisao;
	}

	/**
	 * @param revisao
	 *            the revisao to set
	 */
	public void setRevisao(Integer revisao) {
		this.revisao = revisao;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getNome() + " - Versão: " + getVersao();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((artefato == null) ? 0 : artefato.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + ((revisao == null) ? 0 : revisao.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		result = prime * result + ((versao == null) ? 0 : versao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtefatoPreenchido other = (ArtefatoPreenchido) obj;
		if (artefato == null) {
			if (other.artefato != null)
				return false;
		} else if (!artefato.equals(other.artefato))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (revisao == null) {
			if (other.revisao != null)
				return false;
		} else if (!revisao.equals(other.revisao))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		if (versao == null) {
			if (other.versao != null)
				return false;
		} else if (!versao.equals(other.versao))
			return false;
		return true;
	}

}
