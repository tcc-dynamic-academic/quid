package br.ueg.unucet.quid.anotations;

/**
 * Anotação responsável por dizer que o Field não deve ser gerado
 * componente para preenchimento na visão
 * 
 * @author Diego
 *
 */
public @interface SemComponente {

}
