package br.ueg.unucet.quid.interfaces;

import java.util.Collection;

import br.ueg.unucet.quid.dominios.Artefato;
import br.ueg.unucet.quid.dominios.ArtefatoServico;
import br.ueg.unucet.quid.dominios.Retorno;
import br.ueg.unucet.quid.dominios.Servico;
import br.ueg.unucet.quid.extensao.enums.MomentosDisparoServicoEnum;

/**
 * Interface que representa as operacoes de manipulacao da entidade ArtefatoServico no controlador.
 * @author QUID
 *
 */
public interface IArtefatoServicoControle<T, oid> extends IControle<T, oid>{
	
	/**Metodo responsavel por pesquisar os ArtefatosServico de um artefato
	 * @param artefato Artefao a ser pesquisado
	 * @return Lista de ArtefatosServicos do artefato
	 */
	Retorno<String, Collection<T>> pesquisarArtefatosServicoArtefato(Artefato artefato);

	Retorno<Object, Object> mapearServicoArtefato(
			ArtefatoServico artefatoServicoAMapear);

	Retorno<String, Collection<T>> pesquisarArtefatosServicoPorMomento(
			Artefato artefato, Servico servico,
			MomentosDisparoServicoEnum momentoDisparo);

	Retorno<Object, Object> alterarServicoArtefato(
			ArtefatoServico artefatoServicoAMapear);

	Retorno<String, Collection<ArtefatoServico>> pesquisarArtefatosServicoArtefato(
			Long artefato);
}
