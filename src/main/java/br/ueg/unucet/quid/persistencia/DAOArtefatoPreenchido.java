package br.ueg.unucet.quid.persistencia;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.ueg.unucet.quid.dominios.ArtefatoPreenchido;
import br.ueg.unucet.quid.interfaces.IDAOArtefatoPreenchido;

@Repository
public class DAOArtefatoPreenchido extends
		DAOGenerica<ArtefatoPreenchido, Long> implements IDAOArtefatoPreenchido<ArtefatoPreenchido, Long> {

}
