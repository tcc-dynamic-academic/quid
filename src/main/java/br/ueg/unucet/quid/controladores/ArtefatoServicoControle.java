package br.ueg.unucet.quid.controladores;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.ueg.unucet.quid.dominios.Artefato;
import br.ueg.unucet.quid.dominios.ArtefatoServico;
import br.ueg.unucet.quid.dominios.MembroFramework;
import br.ueg.unucet.quid.dominios.Retorno;
import br.ueg.unucet.quid.dominios.Servico;
import br.ueg.unucet.quid.enums.TipoErroEnum;
import br.ueg.unucet.quid.excessoes.QuidExcessao;
import br.ueg.unucet.quid.excessoes.ServicoExcessao;
import br.ueg.unucet.quid.extensao.enums.MomentosDisparoServicoEnum;
import br.ueg.unucet.quid.interfaces.IArtefatoServicoControle;
import br.ueg.unucet.quid.interfaces.IDAO;
import br.ueg.unucet.quid.interfaces.IDAOArtefatoServico;
import br.ueg.unucet.quid.utilitarias.FabricaProperties;
import br.ueg.unucet.quid.utilitarias.LeitoraPropertiesUtil;

/**
 * @author QUID Classe responsavel por realizar as operacoes sobre a enditdade
 *         ArtefatoServico
 */
@Service("ArtefatoServicoControle")
public class ArtefatoServicoControle extends
		GenericControle<ArtefatoServico, Long> implements
		IArtefatoServicoControle<ArtefatoServico, Long> {
	
	protected LeitoraPropertiesUtil propertiesMensagensUtil = FabricaProperties.loadMessages();

	/**
	 * Atributo responsavel por realizar as operacoes de persistencia.
	 */
	@Autowired
	private IDAOArtefatoServico<ArtefatoServico, Long> daoArtefatoServico;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ueg.unucet.quid.interfaces.IArtefatoServicoControle#
	 * pesquisarArtefatosServicoArtefato(br.ueg.unucet.quid.dominios.Artefato)
	 */
	public Retorno<String, Collection<ArtefatoServico>> pesquisarArtefatosServicoPorMembro(
			Artefato artefato, Servico servico, MembroFramework membroFramework) {
		ArtefatoServico artefatoServico = new ArtefatoServico();
		artefatoServico.setArtefato(artefato);
		artefatoServico.setServico(servico);
		artefatoServico.setMembroFramework(membroFramework);
		
		//Camada Serviço
		Collection<ArtefatoServico> listaArtefatoServico = pesquisarPorRestricao(artefatoServico, new String[] {
				"artefatoservico.codigo", "artefatoservico.parametrosServico","artefatoservico.momentoDisparo",
				"artefatoservico.membroFramework",
				"artefatoservico.artefato.codigo",
				"artefatoservico.servico.codigo",
				"artefatoservico.servico.nome",
				"artefatoservico.servico.versao",
				"artefatoservico.servico.revisao",
				"artefatoservico.servico.descricao",
				"artefatoservico.anteriorObrigatorio",
				"artefatoservico.servicoAnterior.codigo",
				"artefatoservico.servicoAnterior.nome",
				"artefatoservico.servicoAnterior.versao",
				"artefatoservico.servicoAnterior.revisao",
				"artefatoservico.servicoProximo.codigo",
				"artefatoservico.servicoProximo.nome",
				"artefatoservico.servicoProximo.versao",
				"artefatoservico.servicoProximo.revisao" });
		Retorno<String, Collection<ArtefatoServico>> retorno = new Retorno<String, Collection<ArtefatoServico>>();
		retorno.adicionarParametro(Retorno.PARAMERTO_LISTA, listaArtefatoServico);
		if(listaArtefatoServico.isEmpty()){
			retorno.setSucesso(false);
			retorno.setTipoErro(TipoErroEnum.INFORMATIVO);
			retorno.setMensagem(propertiesMensagensUtil.getValor("lista_vazia"));
		}else{
			retorno.setSucesso(true);
		}
		return retorno;
		//END Camada Serviço
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ueg.unucet.quid.interfaces.IArtefatoServicoControle#
	 * pesquisarArtefatosServicoArtefato(br.ueg.unucet.quid.dominios.Artefato)
	 */
	public Retorno<String, Collection<ArtefatoServico>> pesquisarArtefatosServicoPorMomento(
			Artefato artefato, Servico servico, MomentosDisparoServicoEnum momentoDisparo) {
		ArtefatoServico artefatoServico = new ArtefatoServico();
		artefatoServico.setArtefato(artefato);
		artefatoServico.setServico(servico);
		artefatoServico.setMomentoDisparo(momentoDisparo);
		//CAMADA SERVICO
		Collection<ArtefatoServico> listaArtefatoServico = pesquisarPorRestricao(artefatoServico, new String[] {
				"artefatoservico.codigo", "artefatoservico.parametrosServico","artefatoservico.momentoDisparo",
				"artefatoservico.artefato.codigo",
				"artefatoservico.servico.codigo",
				"artefatoservico.servico.nome",
				"artefatoservico.servico.versao",
				"artefatoservico.servico.revisao",
				"artefatoservico.servico.descricao",
				"artefatoservico.anteriorObrigatorio",
				"artefatoservico.servicoAnterior.codigo",
				"artefatoservico.servicoAnterior.nome",
				"artefatoservico.servicoAnterior.versao",
				"artefatoservico.servicoAnterior.revisao",
				"artefatoservico.servicoProximo.codigo",
				"artefatoservico.servicoProximo.nome",
				"artefatoservico.servicoProximo.versao",
				"artefatoservico.servicoProximo.revisao" });
		Retorno<String, Collection<ArtefatoServico>> retorno = new Retorno<String, Collection<ArtefatoServico>>();
		retorno.adicionarParametro(Retorno.PARAMERTO_LISTA, listaArtefatoServico);
		if(listaArtefatoServico.isEmpty()){
			retorno.setSucesso(false);
			retorno.setTipoErro(TipoErroEnum.INFORMATIVO);
			retorno.setMensagem(propertiesMensagensUtil.getValor("lista_vazia"));
		}else{
			retorno.setSucesso(true);
		}
		return retorno;
		//END Camada Serviço
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ueg.unucet.quid.interfaces.IArtefatoServicoControle#
	 * pesquisarArtefatosServicoArtefato(br.ueg.unucet.quid.dominios.Artefato)
	 */
	public Retorno<String, Collection<ArtefatoServico>> pesquisarArtefatosServicoArtefato(
			Artefato artefato) {
		ArtefatoServico artefatoServico = new ArtefatoServico();
		artefatoServico.setArtefato(artefato);
		///CAMADA SERVICO
		Collection<ArtefatoServico> listaArtefatoServico =  pesquisarPorRestricao(artefatoServico, new String[] {
				"artefatoservico.codigo", "artefatoservico.parametrosServico",
				"artefatoservico.artefato.codigo",
				"artefatoservico.servico.codigo",
				"artefatoservico.servico.nome",
				"artefatoservico.servico.versao",
				"artefatoservico.servico.revisao",
				"artefatoservico.servico.descricao",
				"artefatoservico.anteriorObrigatorio",
				"artefatoservico.servicoAnterior.codigo",
				"artefatoservico.servicoAnterior.nome",
				"artefatoservico.servicoAnterior.versao",
				"artefatoservico.servicoAnterior.revisao",
				"artefatoservico.servicoProximo.codigo",
				"artefatoservico.servicoProximo.nome",
				"artefatoservico.servicoProximo.versao",
				"artefatoservico.servicoProximo.revisao" });
		Retorno<String, Collection<ArtefatoServico>> retorno = new Retorno<String, Collection<ArtefatoServico>>();
		retorno.adicionarParametro(Retorno.PARAMERTO_LISTA, listaArtefatoServico);
		if(listaArtefatoServico.isEmpty()){
			retorno.setSucesso(false);
			retorno.setTipoErro(TipoErroEnum.INFORMATIVO);
			retorno.setMensagem(propertiesMensagensUtil.getValor("lista_vazia"));
		}else{
			retorno.setSucesso(true);
		}
		return retorno;
		//END Camada Serviço
	}

	@Override
	public IDAO<ArtefatoServico, Long> getDao() {
		return daoArtefatoServico;
	}

	// GETTERS AND SETTERS

	public IDAOArtefatoServico<ArtefatoServico, Long> getDaoArtefatoServico() {
		return daoArtefatoServico;
	}

	public void setDaoArtefatoServico(
			IDAOArtefatoServico<ArtefatoServico, Long> daoArtefatoServico) {
		this.daoArtefatoServico = daoArtefatoServico;
	}

	@Transactional(value = "transactionManager1", propagation=Propagation.REQUIRED, noRollbackFor=Exception.class)
	public Retorno<Object,Object> mapearServicoArtefato(ArtefatoServico artefatoServico)
			 {
		Retorno<Object,Object> retorno = new Retorno<Object,Object>();
		
		// TODO validar parâmetros do ArtefatoSertivo
		try {
			inserir(artefatoServico);
			retorno.setSucesso(true);
		} catch (QuidExcessao e) {
			try{
			throw new ServicoExcessao(
					this.propertiesMessagesUtil.getValor("erro_insercao"));
			} catch (ServicoExcessao e1) {
				retorno = (Retorno<Object,Object>) construirRetornoErro(retorno,e,TipoErroEnum.ERRO_SIMPLES);
			}
		}
		return retorno;

	}
	

	@Transactional(value = "transactionManager1", propagation=Propagation.REQUIRED, noRollbackFor=Exception.class)
	public Retorno<Object,Object>  alterarServicoArtefato(ArtefatoServico artefatoServico) {
		Retorno<Object,Object> retorno = new Retorno<Object,Object>();
		
		// TODO validar parâmetros do ArtefatoSertivo
		try {
			alterar(artefatoServico);
			retorno.setSucesso(true);
		} catch (QuidExcessao e) {
			try {
				throw new ServicoExcessao(
						this.propertiesMessagesUtil.getValor("erro_update"));
			} catch (ServicoExcessao e1) {
				retorno = (Retorno<Object,Object>) construirRetornoErro(retorno,e,TipoErroEnum.ERRO_SIMPLES);
			}
		}
		return retorno;

	}


	@Override
	public Retorno<String, Collection<ArtefatoServico>> pesquisarArtefatosServicoArtefato(
			Long artefato) {
		Artefato artefatoInstanciado = new Artefato();
		artefatoInstanciado.setCodigo(artefato);
		
		return pesquisarArtefatosServicoArtefato(artefatoInstanciado);
	}

}
